package com.crasher.asteroid.db

import android.content.ContentValues
import android.content.Context
import android.database.sqlite.SQLiteDatabase
import android.database.sqlite.SQLiteOpenHelper
import android.util.Log
import java.util.*

class DbHelper(c: Context) : SQLiteOpenHelper(c, "Hunters", null, DATA_BASE_VERSION) {
    private var currentDB: SQLiteDatabase? = null

    // ----------------------------
    val volumeLevel: Int
        @Synchronized get() {
            var volumeLevel = -1

            val q = "SELECT value FROM $TABLE_SETTINGS WHERE name='volume'"
            val cur = currentDB!!.rawQuery(q, null)
            if (cur != null && cur.moveToFirst()) {
                volumeLevel = Integer.parseInt(cur.getString(0))
                cur.close()
            }

            return volumeLevel
        }
    val points: Int
        @Synchronized get() {
            var points = 0
            val q = "SELECT points FROM $TABLE_POINTS WHERE points > 0 ORDER BY points LIMIT 1"
            val cur = currentDB!!.rawQuery(q, null)
            if (cur != null && cur.moveToFirst()) {
                points = Integer.parseInt(cur.getString(0))
                cur.close()
            }

            return points

        }

    init {
        Log.i(TAG, "dbhelper constructor")
        initDatabase()
    }

    override fun onCreate(db: SQLiteDatabase) {
        createTables(db)
    }

    override fun onUpgrade(db: SQLiteDatabase, arg1: Int, arg2: Int) {
        dropTable(db, TABLE_SETTINGS)
        createTables(db)
    }


    private fun getCreateTableQuery(tableName: String,
                                    map: HashMap<String, String>): String {
        var query = ("CREATE TABLE IF NOT EXISTS " + tableName
                + " (_id INTEGER PRIMARY KEY  AUTOINCREMENT, ")
        val keys = map.keys
        var i = 0
        for (key in keys) {
            query += key + " " + map[key]
            if (i < map.size - 1)
                query += ", "
            i++
        }
        query += ");"

        return query
    }

    private fun createTables(db: SQLiteDatabase) {

        var map: HashMap<String, String>

        // --- Settings
        map = HashMap()
        map["name"] = TEXT
        map["value"] = TEXT
        createTable(db, getCreateTableQuery(TABLE_SETTINGS, map))
        // --- Settings
        map = HashMap()
        map["points"] = INTEGER
        map["date"] = INTEGER
        createTable(db, getCreateTableQuery(TABLE_POINTS, map))
    }

    // ----------------------------
    fun createTable(db: SQLiteDatabase, query: String) {
        db.execSQL(query)
    }

    fun dropTable(db: SQLiteDatabase, table: String) {
        db.execSQL("DROP TABLE IF EXISTS $table")
    }

    @Synchronized
    fun initDatabase() {
        currentDB = writableDatabase
    }

    fun getDatabase(isReadable: Boolean): SQLiteDatabase? {
        return currentDB
    }

    // ----------------------------
    @Synchronized
    fun insertSettings(name: String, value: String) {
        val values: ContentValues

        values = ContentValues()
        values.put("name", name)
        values.put("value", value)

        val update = volumeLevel != -1

        if (update)
            currentDB!!.update(TABLE_SETTINGS, values, "name='$name'", null)
        else
            currentDB!!.insert(TABLE_SETTINGS, null, values)
    }


    // ----------------------------

    @Synchronized
    fun insertPoints(points: Int) {
        val values: ContentValues
        values = ContentValues()
        values.put("points", points)
        values.put("date", Date().time)
        currentDB!!.insert(TABLE_POINTS, null, values)
    }

    companion object {
        private val DATA_BASE_VERSION = 3
        private val TAG = DbHelper::class.java.simpleName

        private val BLOB = "blob"
        private val TEXT = "TEXT"
        private val INTEGER = "INTEGER"

        val TABLE_SETTINGS = "settings"
        val TABLE_POINTS = "points"

        lateinit var instance: DbHelper

        fun init(c: Context) {
            instance = DbHelper(c)
        }
    }
}
