package com.crasher.asteroid

import android.app.Application
import android.content.Context
import android.util.DisplayMetrics
import android.view.WindowManager
import com.crasher.asteroid.db.DbHelper

class App : Application() {


    override fun onCreate() {
        super.onCreate()
        DbHelper.init(this)
        val wm = getSystemService(Context.WINDOW_SERVICE) as WindowManager

        val metrics = DisplayMetrics()
        wm.defaultDisplay.getMetrics(metrics)

        MUTLI_PLAYER = metrics.heightPixels / 480.0
        MainActivity.WIDHT = metrics.widthPixels
        MainActivity.HEIGHT = metrics.heightPixels
    }

    companion object {
        var MUTLI_PLAYER: Double = 0.toDouble()
    }
}
