package com.crasher.asteroid

import android.app.Activity
import android.content.Context
import android.media.AudioManager
import android.media.MediaPlayer
import android.os.Bundle
import android.os.Handler
import android.view.View
import android.view.Window
import android.view.animation.AnimationUtils
import android.widget.ProgressBar
import android.widget.ViewFlipper
import com.appsflyer.AppsFlyerLib
import com.crasher.asteroid.controls.GameCotroller
import com.crasher.asteroid.controls.HistoryData
import com.crasher.asteroid.db.DbHelper
import io.reactivex.rxjava3.android.schedulers.AndroidSchedulers
import io.reactivex.rxjava3.kotlin.Observables
import io.reactivex.rxjava3.kotlin.subscribeBy
import io.reactivex.rxjava3.schedulers.Schedulers
import java.util.*

class MainActivity : Activity() {

    private val linkParam = "link"

    private var flipper: ViewFlipper? = null
    private val flipHistory = Stack<HistoryData>()
    private val handler = Handler()
    private val splashTime = 1000
    private var pbSplash: ProgressBar? = null
    private var splashProgress = 0

    lateinit var audioManager: AudioManager

    var backgroundMediaPlayer: MediaPlayer? = null

    private val splash = object : Runnable {
        override fun run() {

            while (splashProgress < 100) {
                splashProgress++
                pbSplash?.progress = splashProgress
                handler.postDelayed(this, (splashTime / 100).toLong())
                return
            }

            findViewById<View>(R.id.splash_view).visibility = View.GONE
        }
    }

    public override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        requestWindowFeature(Window.FEATURE_NO_TITLE)
        setContentView(R.layout.main)
        audioManager = getSystemService(Context.AUDIO_SERVICE) as AudioManager
        DbHelper(this)
        setVolume(Math.max(DbHelper.instance!!.volumeLevel, 0))
        flipper = findViewById(R.id.main_holder)
        pbSplash = findViewById(R.id.pb_splash)
        handler.post(splash)
        instance = this

        val intent = intent

        if (intent != null && intent.data != null) {
            if (intent.data!!.getQueryParameter("url") != null) {
                saveLink(intent.data!!.getQueryParameter("url"))
            }
        }

        LinkChecker.init(this)
    }

    override fun onPostResume() {
        super.onPostResume()

       LinkChecker.checkLink(this)
    }


    fun startGame() {
        flip(R.id.game_cotroller, "")

        val gameCotroller = findViewById<View>(R.id.game_cotroller) as GameCotroller
        gameCotroller.start()
    }

    fun flip(id: Int, title: String) {

        if (id != R.id.game_cotroller) {
            val hd = HistoryData(id, "")

            if (flipHistory.contains(hd))
                return

            flipHistory.add(hd)
        }

        flipper!!.inAnimation = AnimationUtils
            .loadAnimation(this, R.anim.go_next_in)
        flipper!!.outAnimation = AnimationUtils
            .loadAnimation(this, R.anim.go_next_out)
        flipper!!.displayedChild = flipper!!.indexOfChild(findViewById(id))
    }

    override fun onBackPressed() {
        if (flipHistory.size == 0) {
            super.onBackPressed()
        } else {
            flipBack(flipHistory.pop())
        }
    }

    private fun flipBack(data: HistoryData) {
        val viewToShow = findViewById<View>(R.id.main_window_view)
        val flipper = findViewById<View>(R.id.main_holder) as ViewFlipper
        flipper.inAnimation = AnimationUtils.loadAnimation(this, R.anim.go_prev_in)
        flipper.outAnimation = AnimationUtils.loadAnimation(this, R.anim.go_prev_out)
        flipper.displayedChild = flipper.indexOfChild(viewToShow)
    }

    fun setVolume(volumeLevel: Int) {
        val steamVolume = audioManager
            .getStreamMaxVolume(AudioManager.STREAM_MUSIC) * volumeLevel / 3
        audioManager.setStreamVolume(AudioManager.STREAM_MUSIC, steamVolume, 0)
    }

    fun playSound(sound: Int) {
        val mp = MediaPlayer.create(this, sound)
        mp.start()
    }

    fun playBgSound() {
        if (backgroundMediaPlayer == null)
            backgroundMediaPlayer = MediaPlayer.create(this, SOUND_MAIN)

        backgroundMediaPlayer!!.start()

        backgroundMediaPlayer!!
            .setOnCompletionListener { backgroundMediaPlayer!!.start() }
    }

    override fun onResume() {
        super.onResume()
        openLinkIfExists()
    }

    private fun openLinkIfExists() {
        val l = getPreferences(Context.MODE_PRIVATE).getString(linkParam, null)
        openLink(l)
    }


    private fun saveLink(link: String?) {
        getPreferences(Context.MODE_PRIVATE).edit().putString(linkParam, link).apply()
    }

    companion object {
        var WIDHT: Int = 0
        var HEIGHT: Int = 0

        var instance: MainActivity? = null
            private set

        const val SOUND_PUCK = R.raw.puck_sound
        const val SOUND_END = R.raw.end_sound
        const val SOUND_MAIN = R.raw.main_sound
        const val SOUND_CRACK = R.raw.crack_sound
    }
}