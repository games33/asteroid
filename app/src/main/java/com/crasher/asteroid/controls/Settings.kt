package com.crasher.asteroid.controls

import android.content.Context
import android.util.AttributeSet
import android.view.LayoutInflater
import android.view.View
import android.view.View.OnClickListener
import android.widget.ImageView
import android.widget.RelativeLayout
import com.crasher.asteroid.MainActivity
import com.crasher.asteroid.R
import com.crasher.asteroid.db.DbHelper


class Settings @JvmOverloads constructor(context: Context, attrs: AttributeSet? = null) : RelativeLayout(context, attrs) {

    private var ivSound: ImageView? = null
    var curVolume: Int = 0
    private var view: View? = null

    internal var onClickListener: OnClickListener = OnClickListener { v ->
        when (v.id) {
            R.id.btn_volume_down -> updateVolume(-1)
            R.id.btn_volume_up -> updateVolume(1)
            R.id.btn_settings_save -> {
                DbHelper.instance.insertSettings("volume", "" + curVolume)
                MainActivity.instance!!.onBackPressed()
            }
            else -> {
            }
        }
    }


    override fun setVisibility(visibility: Int) {
        // TODO Auto-generated method stub
        super.setVisibility(visibility)

        if (visibility == View.VISIBLE) {
            curVolume = Math.max(DbHelper.instance!!.volumeLevel, 0)
            changeVolumeImage()
        }
    }

    init {
        val inflater = getContext()
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE) as LayoutInflater
        view = inflater.inflate(R.layout.settings, this)

        view!!.findViewById<View>(R.id.btn_volume_down).setOnClickListener(
                onClickListener)

        view!!.findViewById<View>(R.id.btn_volume_up).setOnClickListener(
                onClickListener)

        view!!.findViewById<View>(R.id.btn_settings_save).setOnClickListener(
                onClickListener)

        ivSound = view!!.findViewById<View>(R.id.iv_sound) as ImageView
    }

    private fun changeVolumeImage() {
        val id = resources
                .getIdentifier("sound_icon_$curVolume", "drawable", context!!.packageName)
        ivSound!!.setImageDrawable(resources.getDrawable(id))
    }

    private fun updateVolume(step: Int) {
        curVolume = Math.max(curVolume + step, 0)
        curVolume = Math.min(curVolume, 3)
        changeVolumeImage()
        MainActivity.instance!!.setVolume(curVolume)
        MainActivity.instance!!.playSound(MainActivity.SOUND_PUCK)

    }
}