package com.crasher.asteroid.controls

import android.graphics.Bitmap
import android.graphics.BitmapFactory
import android.graphics.Canvas
import android.graphics.Rect
import com.crasher.asteroid.MainActivity.Companion.HEIGHT
import com.crasher.asteroid.MainActivity.Companion.WIDHT
import com.crasher.asteroid.R
import com.crasher.asteroid.App.Companion.MUTLI_PLAYER

class GameBackground(field: GameField, second: Boolean) : IAnimatedElement {

    val source: Bitmap = BitmapFactory
            .decodeResource(field.resources, R.drawable.playground_game)


    var y: Int = 0

    private val h: Int = source.height
    private val w: Int = source.width

    override
    val width: Int
        get() = w

    override
    val height: Int
        get() = h


    override fun render(canvas: Canvas) {
        if (y < HEIGHT)
            return
        val destRect = Rect(0, 0, WIDHT, HEIGHT)
        val target = Rect(0, y - HEIGHT, WIDHT, y)
        canvas.drawBitmap(source, target, destRect, null)
    }

    override fun update() {
        y -= STEP.toInt()

        if (y < 0) {
            y = h - HEIGHT
        }
    }

    override fun remove() {

    }

    companion object {
        private val STEP = 5 * MUTLI_PLAYER
    }

}
