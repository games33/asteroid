package com.crasher.asteroid.controls

import android.graphics.Bitmap
import android.graphics.BitmapFactory
import android.graphics.Canvas
import android.graphics.Rect
import com.crasher.asteroid.MainActivity.Companion.HEIGHT
import com.crasher.asteroid.MainActivity.Companion.WIDHT

import com.crasher.asteroid.R
import com.crasher.asteroid.App.Companion.MUTLI_PLAYER

class Enemy(internal var field: GameField) : IAnimatedElement {

    // ------
    internal var state: Int = 0
    // ------
    private var creationFrames: Array<Bitmap>? = null
    private val explosionFrames: Array<Bitmap>
    internal var x: Int = 0
    internal var y: Int = 0
    // ------
    private var currentBitmap: Bitmap? = null

    private var currentAnimationFrame = 0
    private var maxAnimationFrames: Int = 0

    override val width: Int
        get() = currentBitmap!!.width

    override val height: Int
        get() = currentBitmap!!.height

    init {

        val creationFramesIds = intArrayOf(R.drawable.enemy_create_1, R.drawable.enemy_create_2, R.drawable.enemy_create_3)
        creationFrames = creationFramesIds.map { BitmapFactory.decodeResource(field.resources, it) }.apply {
            currentBitmap = get(0)
        }.toTypedArray()


        maxAnimationFrames = creationFramesIds.size


        val framesIds = intArrayOf(R.drawable.die_1, R.drawable.die_2, R.drawable.die_3, R.drawable.die_4, R.drawable.die_5, R.drawable.die_6, R.drawable.die_7)
        explosionFrames = framesIds.map {
            BitmapFactory.decodeResource(field.resources, it)
        }.toTypedArray()
        changeState(STATE_CREATE)
        x = (currentBitmap!!.width / 2 + Math.random() * (WIDHT - currentBitmap!!.width)).toInt()
        y = 30
    }

    override fun render(canvas: Canvas) {
        val destRect = Rect(x - currentBitmap!!.width / 2, y - 60, x + currentBitmap!!.width / 2, y + currentBitmap!!.height)
        canvas.drawBitmap(currentBitmap!!, null, destRect, null)
    }

    override fun update() {

        when (state) {
            STATE_CREATE -> if (currentAnimationFrame == maxAnimationFrames) {
                changeState(STATE_IDLE)
            } else {
                currentBitmap = creationFrames!![currentAnimationFrame]
                currentAnimationFrame++
            }
            STATE_DIE -> if (currentAnimationFrame == maxAnimationFrames) {
                remove()
                return
            } else {
                currentBitmap = explosionFrames[currentAnimationFrame]
                currentAnimationFrame++
            }
        }
        if (state != STATE_DIE)
            y += STEP.toInt()

        if (y - currentBitmap!!.height - 100 >= HEIGHT) {
            remove()
            field.lifeDown()
        }

    }

    fun die() {
        currentBitmap = explosionFrames[0]
        maxAnimationFrames = explosionFrames.size
        changeState(STATE_DIE)
    }

    private fun changeState(newState: Int) {
        if (newState == state)
            return
        if (newState == STATE_CREATE || newState == STATE_DIE) {
            currentAnimationFrame = 0
        }
        if (newState == STATE_IDLE)
            currentBitmap = BitmapFactory
                    .decodeResource(field.resources, R.drawable.enemy_create_3)

        state = newState
    }

    override fun remove() {
        field.remove(this)
        creationFrames = null
        currentBitmap = null
    }

    companion object {
        private val STATE_CREATE = -1
        private val STATE_IDLE = 0
        val STATE_DIE = 1
        // ------
        private val STEP = 3 * MUTLI_PLAYER
    }
}
