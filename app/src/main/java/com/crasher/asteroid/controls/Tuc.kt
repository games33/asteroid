package com.crasher.asteroid.controls

import android.content.Context
import android.graphics.Bitmap
import android.graphics.BitmapFactory
import android.graphics.Canvas
import android.graphics.Rect
import com.crasher.asteroid.MainActivity
import com.crasher.asteroid.MainActivity.Companion.HEIGHT
import com.crasher.asteroid.MainActivity.Companion.WIDHT
import com.crasher.asteroid.R
import com.crasher.asteroid.App.Companion.MUTLI_PLAYER

class Tuc(context: Context, private val field: GameField) : IAnimatedElement {

    private val idleView: Bitmap
    private val creationAnimation: Array<Bitmap>
    private val shootAnimation: Array<Bitmap>

    var state = 0

    private var currentTuc: Bitmap? = null

    internal var currentAnimationFrame = 0
    internal var maxAnimationFrames: Int = 0
    internal var resource: IntArray? = null

    private var x: Int = 0
    var y: Int = 0
        get() = HEIGHT - currentTuc!!.height
    private var moveTo: Int = 0

    override val width: Int
        get() = currentTuc!!.width

    override val height: Int
        get() = currentTuc!!.height

    init {

        y = MainActivity.HEIGHT

        idleView = BitmapFactory
                .decodeResource(context.resources, R.drawable.shoot_1)
        val creationAnimationIds = intArrayOf(R.drawable.create_1, R.drawable.create_1, R.drawable.create_2, R.drawable.create_2, R.drawable.create_3, R.drawable.create_3, R.drawable.create_4, R.drawable.create_4)
        creationAnimation = creationAnimationIds.map {
            BitmapFactory.decodeResource(context.resources, it)
        }.toTypedArray()
        // ---
        val shootAnimationIds = intArrayOf(R.drawable.shoot_1, R.drawable.shoot_2, R.drawable.shoot_3, R.drawable.shoot_4, R.drawable.shoot_5, R.drawable.shoot_6, R.drawable.shoot_7, R.drawable.shoot_8)
        shootAnimation = shootAnimationIds.map {
            BitmapFactory.decodeResource(context.resources, it)
        }.toTypedArray()
        changeState(STATE_CREATE)
        maxAnimationFrames = creationAnimation.size

        x = WIDHT / 2 - creationAnimation[0].width / 2
        moveTo = x
    }

    override fun render(canvas: Canvas) {
        if (currentTuc == null)
            return
        val w = currentTuc!!.width
        val h = currentTuc!!.height

        val destRect = Rect(x, HEIGHT - h, x + w, HEIGHT)
        canvas.drawBitmap(currentTuc!!, null, destRect, null)
    }

    override fun update() {
        when (state) {
            STATE_IDLE -> shootDelay--
            STATE_CREATE -> {
                currentTuc = creationAnimation[currentAnimationFrame]
                currentAnimationFrame++
                if (currentAnimationFrame == maxAnimationFrames - 1) {
                    changeState(STATE_IDLE)
                }
            }

            STATE_SHOOT -> {
                currentTuc = shootAnimation[currentAnimationFrame]
                currentAnimationFrame++
                if (currentAnimationFrame == maxAnimationFrames - 1) {
                    changeState(STATE_IDLE)
                    field.shoot(x + currentTuc!!.width / 2, HEIGHT - currentTuc!!
                            .height - 60)
                }
            }
        }

        if (shootDelay == 0) {
            changeState(STATE_SHOOT)
            maxAnimationFrames = shootAnimation.size
        }

        if (moveTo > x + STEP) {
            x += STEP.toInt()
        } else if (moveTo < x - STEP) {
            x -= STEP.toInt()
        }
    }

    fun changeState(newState: Int) {
        if (newState == state)
            return
        if (newState == STATE_SHOOT || newState == STATE_CREATE) {
            currentAnimationFrame = 0
            shootDelay = SHOOT_DELAY
        }
        if (newState == STATE_IDLE)
            currentTuc = idleView

        state = newState
    }

    fun setMoveTo(moveTo: Int) {
        this.moveTo = moveTo - currentTuc!!.width / 2
    }

    override fun remove() {
        field.remove(this)

    }

    companion object {

        private val STEP = 7 * MUTLI_PLAYER
        val STATE_IDLE = 0
        val STATE_CREATE = -1
        val STATE_SHOOT = 1

        private val SHOOT_DELAY = 30
        private var shootDelay = 10
    }

}
