package com.crasher.asteroid.controls

import android.graphics.Canvas

interface IAnimatedElement {

    val width: Int
    val height: Int

    fun render(canvas: Canvas)
    fun update()
    fun remove()

}
