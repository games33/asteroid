package com.crasher.asteroid.controls

import android.content.Context
import android.util.AttributeSet
import android.view.LayoutInflater
import android.view.View
import android.view.View.OnClickListener
import android.widget.RelativeLayout
import android.widget.TextView
import com.crasher.asteroid.MainActivity
import com.crasher.asteroid.R

class EndGame : RelativeLayout {

    constructor(context: Context) : super(context) {
        init()
    }

    constructor(context: Context, attrs: AttributeSet) : super(context, attrs) {
        init()
    }

    private var view: View? = null

    private var points: Int = 0


    private val onClickListener: OnClickListener = OnClickListener { v ->
        when (v.id) {
            R.id.btn_main_menu -> MainActivity.instance!!.flip(R.id.main_window_view, "")
            R.id.btn_play_again -> MainActivity.instance!!.startGame()
        }
    }

    fun showResult(points: Int) {
        this.points = points
        val p = points.toString()
        val nulls = "000000"
        var `val` = nulls.substring(0, nulls.length - p.length)
        `val` += p

        val textView = findViewById<View>(R.id.tv_result_value) as TextView
        textView.setText(`val`)
    }


    private fun init() {
        val inflater = context
            .getSystemService(Context.LAYOUT_INFLATER_SERVICE) as LayoutInflater
        view = inflater.inflate(R.layout.end_game, this)

        view!!.findViewById<View>(R.id.btn_main_menu).setOnClickListener(
            onClickListener
        )
        view!!.findViewById<View>(R.id.btn_play_again).setOnClickListener(
            onClickListener
        )
    }
}