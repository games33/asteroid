/**
 *
 */
package com.crasher.asteroid.controls

import android.graphics.Canvas
import android.util.Log
import android.view.SurfaceHolder
import java.text.DecimalFormat


/**
 * @author impaler
 *
 * The Main thread which contains the game loop. The thread must have access to
 * the surface view and holder to trigger events every game tick.
 */
class MainThread(// Surface holder that can access the physical surface
        private val surfaceHolder: SurfaceHolder, // The actual view that handles inputs
        // and draws to the surface
        private val panel: GameField
) : Thread() {

    /* Stuff for stats */
    private val df = DecimalFormat("0.##")  // 2 dp
    // the status time counter
    private var statusIntervalTimer = 0L
    // number of frames skipped since the game started
    private var totalFramesSkipped = 0L
    // number of frames skipped in a store cycle (1 sec)
    private var framesSkippedPerStatCycle = 0L

    // number of rendered frames in an interval
    private var frameCountPerStatCycle = 0
    private var totalFrameCount = 0L
    // the last FPS values
    private var fpsStore: DoubleArray = DoubleArray(FPS_HISTORY_NR)
    // the number of times the stat has been read
    private var statsCount: Long = 0
    // the average FPS since the game started
    private var averageFps = 0.0

    // flag to hold game state
    private var running: Boolean = false

    fun setRunning(running: Boolean) {
        this.running = running
    }

    override fun run() {
        var canvas: Canvas?
        Log.d(TAG, "Starting game loop")
        // initialise timing elements for stat gathering
        initTimingElements()

        var beginTime: Long        // the time when the cycle begun
        var timeDiff: Long        // the time it took for the cycle to execute
        var sleepTime: Int        // ms to sleep (<0 if we're behind)
        var framesSkipped: Int    // number of frames being skipped

        sleepTime = 0

        while (running) {
            if (panel.isPause!!) {
                try {
                    Thread.sleep(50)
                } catch (e: InterruptedException) {
                }

                continue
            }
            canvas = null
            // try locking the canvas for exclusive pixel editing
            // in the surface
            try {
                canvas = this.surfaceHolder.lockCanvas()
                synchronized(surfaceHolder) {
                    beginTime = System.currentTimeMillis()
                    framesSkipped = 0    // resetting the frames skipped
                    // update game state
                    this.panel.update()
                    // render state to the screen
                    // draws the canvas on the panel
                    this.panel.render(canvas!!)
                    // calculate how long did the cycle take
                    timeDiff = System.currentTimeMillis() - beginTime
                    // calculate sleep time
                    sleepTime = (FRAME_PERIOD - timeDiff).toInt()

                    if (sleepTime > 0) {
                        // if sleepTime > 0 we're OK
                        try {
                            // send the thread to sleep for a short period
                            // very useful for battery saving
                            Thread.sleep(sleepTime.toLong())
                        } catch (e: InterruptedException) {
                        }

                    }

                    while (sleepTime < 0 && framesSkipped < MAX_FRAME_SKIPS) {
                        // we need to catch up
                        //						this.panel.update(); // update without rendering
                        sleepTime += FRAME_PERIOD    // add frame period to check if in next frame
                        framesSkipped++
                    }

                    // for statistics
                    framesSkippedPerStatCycle += framesSkipped.toLong()
                    // calling the routine to store the gathered statistics
                    storeStats()
                }
            } finally {
                // in case of an exception the surface is not left in
                // an inconsistent state
                if (canvas != null) {
                    surfaceHolder.unlockCanvasAndPost(canvas)
                }
            }    // end finally
        }
    }

    /**
     * The statistics - it is called every cycle, it checks if time since last
     * store is greater than the statistics gathering period (1 sec) and if so
     * it calculates the FPS for the last period and stores it.
     *
     * It tracks the number of frames per period. The number of frames since
     * the start of the period are summed up and the calculation takes part
     * only if the next period and the frame count is reset to 0.
     */
    private fun storeStats() {
        frameCountPerStatCycle++
        totalFrameCount++
        // assuming that the sleep works each call to storeStats
        // happens at 1000/FPS so we just add it up
        statusIntervalTimer += FRAME_PERIOD.toLong()

        if (statusIntervalTimer >= STAT_INTERVAL) {
            // calculate the actual frames pers status check interval
            val actualFps = (frameCountPerStatCycle / (STAT_INTERVAL / 1000)).toDouble()

            //stores the latest fps in the array
            fpsStore[statsCount.toInt() % FPS_HISTORY_NR] = actualFps

            // increase the number of times statistics was calculated
            statsCount++

            var totalFps = 0.0
            // sum up the stored fps values
            for (i in 0 until FPS_HISTORY_NR) {
                totalFps += fpsStore!![i]
            }

            // obtain the average
            if (statsCount < FPS_HISTORY_NR) {
                // in case of the first 10 triggers
                averageFps = totalFps / statsCount
            } else {
                averageFps = totalFps / FPS_HISTORY_NR
            }
            // saving the number of total frames skipped
            totalFramesSkipped += framesSkippedPerStatCycle
            // resetting the counters after a status record (1 sec)
            framesSkippedPerStatCycle = 0
            statusIntervalTimer = 0
            frameCountPerStatCycle = 0
            //			Log.d(TAG, "Average FPS:" + df.format(averageFps));
            panel.setAvgFps("FPS: " + df.format(averageFps))
        }
    }

    private fun initTimingElements() {
        // initialise timing elements
        for (i in 0 until FPS_HISTORY_NR) {
            fpsStore[i] = 0.0
        }
        Log.d(TAG + ".initTimingElements()", "Timing elements for stats initialised")
    }

    companion object {

        private val TAG = MainThread::class.java!!.getSimpleName()

        // desired fps
        private val MAX_FPS = 60
        // maximum number of frames to be skipped
        private val MAX_FRAME_SKIPS = 5
        // the frame period
        private val FRAME_PERIOD = 1000 / MAX_FPS
        // we'll be reading the stats every second
        private val STAT_INTERVAL = 1000 //ms
        // the average will be calculated by storing
        // the last n FPSs
        private val FPS_HISTORY_NR = 10
    }

}
