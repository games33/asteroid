package com.crasher.asteroid.controls

import android.annotation.SuppressLint
import android.content.Context
import android.graphics.*
import android.util.Log
import android.view.MotionEvent
import android.view.SurfaceHolder
import android.view.SurfaceHolder.Callback
import android.view.SurfaceView
import com.crasher.asteroid.MainActivity
import com.crasher.asteroid.R
import java.util.*

@SuppressLint("ViewConstructor")
class GameField(context: Context, private val controller: GameCotroller) : SurfaceView(context), Callback {

    var drawThread: MainThread? = null

    private val activeElements: ArrayList<IAnimatedElement> = ArrayList()
    private val waitingElemets: ArrayList<IAnimatedElement> = ArrayList()
    private val removingElemets: ArrayList<IAnimatedElement> = ArrayList()

    private var tuc: Tuc? = null

    private var bg: Bitmap? = null

    // the fps to be displayed
    private var avgFps: String? = null

    private var currentFrame = 0

    var isPause: Boolean? = false

    init {
        init()
    }

    private fun init() {
        holder.addCallback(this)
    }

    fun run() {
        bg = BitmapFactory.decodeResource(resources, R.drawable.bg)
    }

    override fun surfaceChanged(arg0: SurfaceHolder, arg1: Int, arg2: Int, arg3: Int) {
    }

    override fun surfaceCreated(arg0: SurfaceHolder) {
        Log.i(TAG, "surfaceCreated")

        tuc = Tuc(context, this).apply {
            waitingElemets.add(this)
        }

        drawThread = MainThread(holder, this)
        drawThread!!.start()
        drawThread!!.setRunning(true)
    }

    override fun surfaceDestroyed(arg0: SurfaceHolder) {
        Log.i(TAG, "surfaceDestroyed")
    }

    fun render(canvas: Canvas) {
        canvas.drawBitmap(bg!!, 0f, 0f, Paint())

        val destRect = Rect(0, 0, MainActivity.WIDHT, MainActivity.HEIGHT)
        canvas.drawBitmap(bg!!, null, destRect, null)

        for (element in activeElements!!) {
            element.render(canvas)
        }

        // display fps
        displayFps(canvas, avgFps)
    }

    fun update() {

        if (tuc!!.state == Tuc.STATE_CREATE) {
            tuc!!.update()
            return
        }

        for (element in activeElements) {
            element.update()
        }

        // coping new elements from temporary to regular
        activeElements.addAll(0, waitingElemets!!)
        // clearing temporary array
        waitingElemets.clear()

        activeElements.removeAll(removingElemets!!)
        removingElemets.clear()

        validateCollisions()

        if (currentFrame == 0) {
            activeElements.add(Enemy(this))

            currentFrame = CREATE_ENEMY_DELAY
        } else {
            currentFrame--
        }
    }

    private fun validateCollisions() {
        val cookies = ArrayList<Cookie>()
        val enemies = ArrayList<Enemy>()

        for (element in activeElements) {
            if (element is Cookie) {
                cookies.add(element)
            } else if (element is Enemy) {
                enemies.add(element)
            }
        }

        for (enemy in enemies) {
            if (enemy.state == Enemy.STATE_DIE)
                continue

            for (cookie in cookies) {
                val cookieCenter = cookie.x + cookie.width / 2
                if (enemy.x - enemy.width / 2 < cookieCenter)
                    if (cookieCenter < enemy.x + enemy.width / 2)
                        if (enemy.y < cookie.y)
                            if (cookie.y < enemy.y + enemy.height) {
                                removingElemets!!.add(cookie)
                                enemy.die()
                                controller.pointsUp()
                            }

            }
        }
    }

    override fun onTouchEvent(event: MotionEvent): Boolean {
        if (event.action == MotionEvent.ACTION_DOWN) {
            tuc!!.setMoveTo(event.x.toInt())
        }
        return true
    }

    fun setAvgFps(avgFps: String) {
        this.avgFps = avgFps
    }

    private fun displayFps(canvas: Canvas?, fps: String?) {
        if (canvas != null && fps != null) {
            val paint = Paint()
            paint.setARGB(255, 255, 255, 255)
            canvas.drawText(fps, (this.width - 50).toFloat(), 20f, paint)
        }
    }

    fun shoot(x: Int, y: Int) {
        val cookie = Cookie(this, x, y)

        // new cookie added to temp array
        waitingElemets!!.add(cookie)

        MainActivity.instance!!.playSound(MainActivity.SOUND_CRACK)
    }

    fun remove(element: IAnimatedElement) {
        removingElemets!!.add(element)
    }

    fun lifeDown() {
        controller.lifeDown()

        tuc!!.changeState(Tuc.STATE_CREATE)
    }

    fun stop() {

        var retry = true
        drawThread!!.setRunning(false)
        while (retry) {
            try {
                drawThread!!.join()
                retry = false
            } catch (e: InterruptedException) {
                Log.i(TAG, "", e)
            }

        }

        drawThread!!.interrupt()

        drawThread = null

        activeElements.clear()
        waitingElemets.clear()
        removingElemets.clear()

        holder.addCallback(null)

        System.gc()

    }

    companion object {

        private val TAG = GameField::class.java.simpleName
        private val CREATE_ENEMY_DELAY = 40
    }
}
