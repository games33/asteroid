package com.crasher.asteroid.controls

import android.content.Context
import android.util.AttributeSet
import android.view.LayoutInflater
import android.view.View
import android.widget.RelativeLayout
import com.crasher.asteroid.R
import com.crasher.asteroid.MainActivity

class Pause : RelativeLayout {
    private var view: View? = null

    internal var onClickListener: View.OnClickListener = OnClickListener { v ->
        val gameController = MainActivity.instance!!.findViewById<View>(R.id.game_cotroller) as GameCotroller
        when (v.id) {
            R.id.btn_main_menu -> {
                MainActivity.instance!!.flip(R.id.main_window_view, "")
                MainActivity.instance!!.backgroundMediaPlayer!!.stop()
                gameController.stopGame()
            }
            R.id.btn_back_to_game -> {
                MainActivity.instance!!.flip(R.id.game_cotroller, "")
                gameController.restoreGame()
            }
            R.id.btn_settings -> MainActivity.instance!!.flip(R.id.settings_view, "")
            else -> {
            }
        }
    }

    constructor(context: Context) : super(context) {
        init()
    }

    constructor(context: Context, attrs: AttributeSet) : super(context, attrs) {
        init()
    }

    private fun init() {
        val inflater = context
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE) as LayoutInflater
        view = inflater.inflate(R.layout.pause, this)

        view!!.findViewById<View>(R.id.btn_main_menu).setOnClickListener(
                onClickListener)
        view!!.findViewById<View>(R.id.btn_settings)
                .setOnClickListener(onClickListener)
        view!!.findViewById<View>(R.id.btn_back_to_game).setOnClickListener(
                onClickListener)

    }
}