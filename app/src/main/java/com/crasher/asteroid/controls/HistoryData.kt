package com.crasher.asteroid.controls

data class HistoryData(val viewId: Int = 0,
                       val title: String)
