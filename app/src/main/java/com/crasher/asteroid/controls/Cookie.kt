package com.crasher.asteroid.controls

import android.graphics.Bitmap
import android.graphics.BitmapFactory
import android.graphics.Canvas
import android.graphics.Rect
import com.crasher.asteroid.R
import com.crasher.asteroid.App.Companion.MUTLI_PLAYER

class Cookie(internal var field: GameField, x: Int, internal var y: Int) : IAnimatedElement {

    private var cookie: Bitmap? = null

    internal var x: Int = 0
    override val width: Int
        get() = cookie!!.width
    override val height: Int
        get() = cookie!!.height

    init {
        cookie = BitmapFactory.decodeResource(field.context.resources,
                R.drawable.cookie)
        this.x = x - cookie!!.width / 2
    }

    override fun render(canvas: Canvas) {
        val destRect = Rect(x, y - 60, x + cookie!!.width, y + cookie!!.height)
        canvas.drawBitmap(cookie!!, null, destRect, null)
    }

    override fun update() {
        y -= COOKIE_STEP.toInt()
    }

    override fun remove() {
        field.remove(this)
        cookie = null
    }

    companion object {

        val COOKIE_STEP = 10 * MUTLI_PLAYER
    }


}
