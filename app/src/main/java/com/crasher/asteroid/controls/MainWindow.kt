package com.crasher.asteroid.controls

import android.content.Context
import android.util.AttributeSet
import android.view.LayoutInflater
import android.view.View
import android.view.View.OnClickListener
import android.widget.RelativeLayout
import com.crasher.asteroid.MainActivity
import com.crasher.asteroid.R

class MainWindow : RelativeLayout {

    private val onClickListener: OnClickListener = OnClickListener { v ->
        if (v.id == R.id.btn_settings)
            MainActivity.instance!!.flip(R.id.settings_view, "")
        if (v.id == R.id.btn_start_game)
            MainActivity.instance!!.startGame()
        if (v.id == R.id.btn_result)
            MainActivity.instance!!.flip(R.id.result_view, "")
    }

    constructor(context: Context) : super(context) {
        init()
    }

    constructor(context: Context, attrs: AttributeSet) : super(context, attrs) {
        init()
    }

    private fun init() {
        val inflater = context
            .getSystemService(Context.LAYOUT_INFLATER_SERVICE) as LayoutInflater
        val view = inflater.inflate(R.layout.main_window, this)

        view.findViewById<View>(R.id.btn_start_game).setOnClickListener(onClickListener)
        view.findViewById<View>(R.id.btn_settings).setOnClickListener(onClickListener)

        view.findViewById<View>(R.id.btn_settings)
            .setOnClickListener(onClickListener)
        view.findViewById<View>(R.id.btn_result).setOnClickListener(onClickListener)
    }
}