package com.crasher.asteroid.controls

import android.content.Context
import android.util.AttributeSet
import android.view.View
import android.view.View.OnClickListener
import android.widget.Button
import android.widget.RelativeLayout
import android.widget.TextView
import com.crasher.asteroid.MainActivity
import com.crasher.asteroid.R
import com.crasher.asteroid.db.DbHelper

class GameCotroller(context: Context, attrs: AttributeSet) : RelativeLayout(context, attrs) {

    private var gameView: GameField? = null
    private var scoreView: View? = null

    lateinit var tvLifes: TextView
    private lateinit var tvPoints: TextView
    lateinit var btnPause: Button

    private var points = 0
    private var lifes: Int = 0

    var onPauseClick: View.OnClickListener = OnClickListener {
        gameView!!.isPause = true
        MainActivity.instance!!.backgroundMediaPlayer!!.pause()
        MainActivity.instance!!.flip(R.id.pause_view, "")
    }

    fun start() {
        try {
            removeAllViews()
        } catch (e: Exception) {
        }

        gameView = GameField(context, this)
        scoreView = View.inflate(context, R.layout.score_panel, null)

        addView(gameView)
        addView(scoreView)

        tvLifes = scoreView!!.findViewById<View>(R.id.tvLifePoints) as TextView
        tvPoints = scoreView!!.findViewById<View>(R.id.tvScore) as TextView
        btnPause = scoreView!!.findViewById<View>(R.id.btnPause) as Button
        btnPause.setOnClickListener(onPauseClick)

        lifes = MAX_LIFE
        points = 0

        setLifePoints(lifes)
        setPoints(points)

        gameView!!.run()

        MainActivity.instance!!.playBgSound()

    }

    fun pointsUp() {
        post {
            points++
            setPoints(points)

            MainActivity.instance!!.playSound(MainActivity.SOUND_PUCK)
        }
    }

    fun lifeDown() {
        post {
            lifes--
            setLifePoints(lifes)

            if (lifes == 0) {
                gameOver()
            }
        }
    }

    private fun setLifePoints(points: Int) {
        tvLifes.text = "x$points"
    }

    private fun setPoints(points: Int) {
        val p = points.toString()
        val nulls = "000000"
        var `val` = nulls.substring(0, nulls.length - p.length)
        `val` += p
        tvPoints.setText(`val`)
    }

    private fun gameOver() {
        DbHelper.instance!!.insertPoints(points)
        MainActivity.instance!!.flip(R.id.end_game_view, "")
        val result = MainActivity.instance!!
            .findViewById<View>(R.id.end_game_view) as EndGame
        result.showResult(points)
        gameView!!.stop()

        MainActivity.instance!!.playSound(MainActivity.SOUND_END)

        MainActivity.instance!!.backgroundMediaPlayer!!.stop()
        MainActivity.instance!!.backgroundMediaPlayer!!.release()

        MainActivity.instance!!.backgroundMediaPlayer = null
    }

    fun restoreGame() {
        MainActivity.instance!!.backgroundMediaPlayer!!.start()
        gameView!!.isPause = false
    }

    fun stopGame() {
        MainActivity.instance!!.backgroundMediaPlayer!!.stop()
        MainActivity.instance!!.backgroundMediaPlayer!!.release()

        MainActivity.instance!!.backgroundMediaPlayer = null

        gameView!!.stop()
    }

    companion object {
        private val MAX_LIFE = 3
    }

}
